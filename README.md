<h1 align="center">task-manager</h1>

Учебный проект в рамках курса по программе JAVA SPRING.

Зеркало: [JSE github.com](https://github.com/KOPTEC-PRGM/jse)

## Требования к software

* Java OpenJDK version "11"
* Apache Maven 3.6.1

## Описание стека технологий

* Java SE 
* Сборщик проектов Apache Maven

## Разработчик

**Потапов Вадим** potapov_vs@nlmk.com

## Команды для сборки приложения

Очистка и удаление каталога `\target`
```
mvn clean
```
Создание каталога `\target` и сборка проекта
```
mvn package
```
## Команда для запуска приложения

```
java -jar target\task-manager-1.0.0.jar
```

## Поддерживаемые терминальные команды

```
help - Вывод списка терминальных команд
version - Информация о версии приложения
about - Информация о разработчике
exit - Выход из приложения
```
